import java.util.Scanner;

public class CalculatorA {
    public static void main(String[] args){

        GeneralMethods.print("Hello! \n");
        GeneralMethods.print("Please enter first number:");
        double firstNum = GeneralMethods.get();

        GeneralMethods.print("Please enter secound number:");
        double secondNum = GeneralMethods.get();

        Scanner scanner = new Scanner(System.in);
        GeneralMethods.print("Please Select operation: + , - , * , /");
        String operation = scanner.next();

        switch (operation){
            case "+":
                GeneralMethods.print("Your Operation result is:" + firstNum + secondNum);
                break;

            case "-":
                GeneralMethods.print("Your Operation result is:" + (firstNum - secondNum));
                break;

            case "*":
                GeneralMethods.print("Your Operation result is:" + firstNum * secondNum);
                break;

            case "/":
                GeneralMethods.print("Your Operation result is:" + firstNum / secondNum);
                break;

            default:
                GeneralMethods.print("Please Select operation: + , - , * , /");
        }
    }
}


import java.util.ArrayList;
import java.util.List;

public class CalculatorC {

    public static void main(String[] args) {

        List<String> array = new ArrayList<>();

        GeneralMethods.print("Hello! \n");

        while (true) {
            GeneralMethods.print("Please enter number:");
            String number = GeneralMethods.getStr();
            array.add(number);

            GeneralMethods.print("Select operation: + , - , * , / , = ");
            String operator = GeneralMethods.getStr();
            array.add(operator);

            if (operator.equals("=")) {
                double result = GeneralMethods.answer(array);

                GeneralMethods.print("Answer is:" + result);
                break;
            }
        }

    }
}
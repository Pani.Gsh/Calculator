import java.util.Scanner;

public class CalculatorB {

    public static void main(String[] args){

        GeneralMethods.print("Hello! \n");
        GeneralMethods.print("Please enter first number:");
        double firstNum = GeneralMethods.get();

        GeneralMethods.print("Please enter second number:");
        double secondNum = GeneralMethods.get();

        Scanner scanner = new Scanner(System.in);
        GeneralMethods.print("Select operation: + , - , * , /");
        String operation = scanner.nextLine();

        if (operation.equals("+")) {
            double result = OperationMethods.add(firstNum, secondNum);
            GeneralMethods.print("Your operation result is \n" + result);
        }

        if (operation.equals("-")) {
            double result = OperationMethods.mines(firstNum, secondNum);
            GeneralMethods.print("Your operation result is \n" + result);
        }

        if (operation.equals("*")) {
            double result = OperationMethods.multiply(firstNum, secondNum);
            GeneralMethods.print("Your operation result is \n" + result);
        }

        if (operation.equals("/")) {
            double result = OperationMethods.divide(firstNum, secondNum);
            GeneralMethods.print("Your operation result is \n" + result);
        }

    }
}

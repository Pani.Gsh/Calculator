public class OperationMethods {


    //return sum of two double var
    public static double add(double firstNum, double secondNum){
        double result = firstNum + secondNum;
        return result;
    }

    //return subtraction of two double var
    public static double mines(double firstNum, double secondNum){
        double result = firstNum - secondNum;
        return result;
    }

    //return Multiplication of two double var
    public static double multiply(double firstNum, double secondNum){
        double result = firstNum * secondNum;
        return result;
    }

    //return divide of two double var
    public static double divide(double firstNum, double secondNum){
        double result = firstNum / secondNum;
        return result;
    }
}

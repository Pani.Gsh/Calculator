

import java.util.List;
import java.util.Scanner;

public class GeneralMethods {

    //print output to show
    public static void print(String str) {
        System.out.println(str);
    }

    //get input from user and convert to double
    public static double get(){
        Scanner scanner = new Scanner(System.in);
            double var = Double.parseDouble(scanner.next());
        return var;
    }

    //get input from user to string
    public static String getStr(){

        Scanner scanner = new Scanner(System.in);
        return scanner.next();
    }

    //get an array and return result of calculations
    public static double answer(List<String> array){

        int i = 0;
        double result =Double.parseDouble(array.get(i));

        for (int j = 2, k = 1; j < array.size(); j += 2){

            double number = Double.parseDouble(array.get(j));

                String operator = array.get(k);
                switch (operator){
                    case "+":
                        result += number;
                        break;


                    case "-":
                        result -= number;
                        break;

                    case "*":
                        result *= number;
                        break;

                    case "/":
                        result /= number;
                        break;

                    default:
                        break;
                }
                k += 2;
        }
        return result;
    }

}





